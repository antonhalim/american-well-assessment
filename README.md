American Well Tech Assessment - Anton Halim

```
Assessment: Write a javascript application that gets data from the following URL and returns the lowest three integers with no duplicates.

https://test.anytime.org/string.txt Also, just a heads up it is HTTPS only.  There is no redirector on the site, so if you forget and only type http:// you will get an error message only.

```
