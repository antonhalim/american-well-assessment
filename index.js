/*
	Assessment: Write a javascript application that gets data from
	the following URL and returns the lowest three integers with no duplicates.
*/

// Require fetching library
const axios = require('axios')

// Source URL
const URL = 'https://test.anytime.org/string.txt'

async function getLowestInteger(url, count = 3) {

	// Store the response text
	const data =  await axios.get(url).then(res => res.data)

	// Split data by new line char
	let result_array = data.split('\n')

	return result_array
		.filter(ele => !isNaN(parseInt(ele))) // Get only numbers from the array
		.sort() // Sort the number ascending
		.slice(0, count) // Take the lowest integers by count
}

getLowestInteger(URL).then(result => console.log(result))
